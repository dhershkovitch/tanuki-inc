const consola = require('consola')

export default function(context) {
  // Only log on the server
  if (!process.server) return

  const dateString = new Date().toISOString()

  consola.log({
    message: `[${dateString}] Resource requested: '${context.req.url}'`
  })
}
